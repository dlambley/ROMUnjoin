/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __riscos
  #include "swis.h"
  #include "kernel.h"
  #include "VersionNum"
#endif

void getimage(char name[], char **image, int *ilen);
void modDump(char *image, char *dirBase);
char *memfind(char *mem, char *find);
void hourglass(int state);

void doexit(void) {
  hourglass(0);
}

int main(int argc, char *argv[]) {
  char *iptr, dirBase[255] = "\0";
  int ilen;

  atexit(doexit);

  fprintf(stdout, "ROM Module Splitter V%s (%s)\n\n", Module_FullVersion, Module_Date);

  if (argc >= 3) {
    hourglass(1);
    strncat(dirBase, argv[2], sizeof(dirBase)-1);
    getimage(argv[1], &iptr, &ilen);
    modDump(iptr, dirBase);
    free(iptr);
    hourglass(0);
  }
  else {
    fprintf(stdout, "Splits up a ROM image into its constituent modules.\n");
    fprintf(stdout, "Each module is saved into an individual file.\nIf there are more than 78 modules in the ROM, they go into subdirectories numbered 1, 2 etcetera.\n\n");
    fprintf(stdout, "Syntax:\tROMUnjoin <ROM image> <dir base>\n\n");
    exit(EXIT_FAILURE);
  }
}

void getimage(char name[], char **image, int *ilenptr)
{
  FILE *fptr;

  if ((fptr = fopen(name, "rb")) == NULL) {
    fprintf(stderr, "Can't open %s\n", name);
    exit(EXIT_FAILURE);
  }

  /* Find size of image */
  fseek(fptr, 0L, SEEK_END);
  *ilenptr = (size_t) ftell(fptr);
  fseek(fptr, 0L, SEEK_SET);

  /* Alloc & read entire image */
  if ((*image = malloc(*ilenptr)) == NULL) {
    fprintf(stderr, "Can't allocate %dK for image\n", (*ilenptr)/1024);
    exit(EXIT_FAILURE);
  }
  if (fread(*image, 1, *ilenptr, fptr) != *ilenptr) {
    fprintf(stderr, "Can't read %s\n", name);
    exit(EXIT_FAILURE);
  }

  fclose(fptr);
}

void modDump(char *image, char *dirBase) {
  char *p, *endPtr;
  int modNum = 1, modSize, fileNum = 1, dirNum = 1, dptr = 0;
  char fileName[256];

  p = memfind(image, "UtilityModule");

  dptr = (p-image-0x14) & ~3;
  while (*((int *)&image[dptr+0x10]) != p-&image[dptr]) {
    dptr -= 4;
  }

  dptr -= 4;

#ifdef __riscos
  sprintf(fileName, "cdir %s", dirBase);
  system(fileName);
#endif

  while (*((int *)&image[dptr]) != 0) {
#ifdef __riscos
    sprintf(fileName, "cdir %s.%d", dirBase, dirNum);
    system(fileName);
#endif
    fileNum = 1;
    while ((fileNum <= 70) && (*((int *)&image[dptr]) != 0)) {
      sprintf(fileName, "%s.%d.Module%03d", dirBase, dirNum, modNum);
      modSize = *(int *)&image[dptr];
      endPtr = (char *)&image[dptr + modSize + 5];

#ifdef __riscos
      _kernel_swi_regs regs;
      regs.r[0] = 11;
      regs.r[1] = (int)&fileName;
      regs.r[2] = 4090;
      regs.r[4] = 0;
      regs.r[5] = (modSize - 1);
      _kernel_swi(OS_File, &regs, &regs);
#endif

      fprintf(stdout,"Writing %7d bytes from %8p to %8p to file %s.\n", modSize, &image[dptr], endPtr, fileName);

#ifdef __riscos
      regs.r[0] = 10;
      regs.r[1] = (int)&fileName;
      regs.r[2] = 4090;
      regs.r[4] = (int)&image[dptr + 4];
      regs.r[5] = (int)endPtr;
      _kernel_swi(OS_File, &regs, &regs);
#else
      FILE *f = 0;
      f = fopen(fileName, "wb");
      if (f==NULL) {
        perror("Could not open output file");
        exit(1);
      }
      fwrite(&image[dptr+4], endPtr - &image[dptr], 1, f);
      fclose(f);
#endif

      dptr += *((int *)&image[dptr]);
      modNum++;
      fileNum++;
    }
    dirNum++;
  }
}

char *memfind(char *mem, char *find)
{
  char *c = mem;

  do
  {
    while (*c != *find)
      c++;

    if (!(strcmp(c, find)))
      return c;

    c++;
  } while (1);
}

/*
 * toggle hourglass
 */
void
hourglass(int state)
{
#ifdef __riscos
  if (state)
    _swix(Hourglass_On,0);
  else
    _swix(Hourglass_Off,0);
#endif
}
